export const setProfile = (payload)=> {
  return {
    type: 'SET_PROFILE',
    payload
  }
}