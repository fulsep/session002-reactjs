import {put, takeEvery} from 'redux-saga/effects'

export function* setProfile(action){
  yield put({type: 'SET_PROFILE', payload: action.payload})
}

export default function*  saga(){
  const log = ()=> {
    console.log('SET PROFILE INVOKED')
  }
  yield takeEvery('SET_PROFILE', log)
}
