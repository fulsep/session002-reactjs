const initialState = {
  data: [
    {
      name: 'Admin',
      email: 'admin@server.com',
      phone: '081231231231'
    },
    {
      name: 'Moderator',
      email: 'mod@server.com',
      phone: '0812312312312'
    }
  ],
}

const profile = (state=initialState, action)=> {
  switch(action.type){
    case 'SET_PROFILE': {
      const data = state.data
      data[action.payload.id] = action.payload.data
      return {
        ...state,
        data
      }
    }
    default : {
      return state
    }
  }
}

export default profile