import {combineReducers} from 'redux'
import profile from './profile'

const reducer = combineReducers({
  profile
})

export default reducer