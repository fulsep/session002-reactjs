import {createStore, applyMiddleware} from 'redux'
import logger from 'redux-logger'
import rootReducer from './reducers'
import createSagaMiddleware from 'redux-saga'
import sagas from './sagas'

const reduxSaga = createSagaMiddleware()

const store = createStore(
  rootReducer,
  applyMiddleware(
    reduxSaga,
    logger
  )
)

reduxSaga.run(sagas)

export default store
