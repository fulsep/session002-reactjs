import React from 'react'
import { Button, Col, Container, Form, FormGroup, Input, Label, Row } from 'reactstrap'
import {connect} from 'react-redux'

// import {setProfile} from '../redux/actions/profile'
import {setProfile} from '../redux/sagas'


const Profile = (props)=>{
  const [id, setId] = React.useState(0)
  const [name, setName] = React.useState('')
  const [email, setEmail] = React.useState('')
  const [phone, setPhone] = React.useState('')

  const saveData = ()=>{
    setProfile({id, data: {name, email, phone}})
  }

  return(
    <Container>
      <Row>
        <Col md={6}>
          <Form>
            <FormGroup>
              <Label>ID</Label>
              <Input name="id" onChange={e=>setId(e.target.value)} value={id} autoComplete/>
            </FormGroup>
            <FormGroup>
              <Label>Name</Label>
              <Input name="name" onChange={e=>setName(e.target.value)} value={name} autoComplete/>
            </FormGroup>
            <FormGroup>
              <Label>Email</Label>
              <Input name="email" onChange={e=>setEmail(e.target.value)} value={email} autoComplete/>
            </FormGroup>
            <FormGroup>
              <Label>Phone</Label>
              <Input name="phone" onChange={e=>setPhone(e.target.value)} value={phone} autoComplete/>
            </FormGroup>
            <Button onClick={saveData} color="primary">Save</Button>
          </Form>
        </Col>
      </Row>
    </Container>
  )
}

export default connect()(Profile)