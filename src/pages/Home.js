import React from 'react'
import { Container, Jumbotron } from 'reactstrap'

const Home = ()=>{
  return(
    <Container>
      <Jumbotron className="mt-5">
        <h1>Hello! This is my first web apps using ReactJS</h1>
      </Jumbotron>
    </Container>
  )
}

export default Home