import React from 'react'
import {connect} from 'react-redux'

const About = (props)=>{
  React.useEffect(()=>{
    console.log(props.profile)
  })
  return(
    <>
      <h1>Our Profile</h1>
      {props.profile.data.map(item => {
        return (
        <React.Fragment key={item.name}>
          <div>Name: {item.name}</div>
          <div>Email: {item.email}</div>
          <div>Phone: {item.phone}</div>
          <div className="mt-5" />
        </React.Fragment>
        )
      })}
    </>
  )
}

const mapStateToProps = state => ({
  profile: state.profile
})

export default connect(mapStateToProps)(About)