import React from 'react'
import {
  BrowserRouter,
  Switch,
  Route
} from 'react-router-dom'
import {Provider} from 'react-redux'

import Home from './pages/Home'
import Contact from './pages/Contact'
import About from './pages/About'
import Profile from './pages/Profile'

import Navbar from './components/NavigationBar'

import store from './redux/store'

const App = ()=>{
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Navbar />
        <Switch>
          <Route component={Home} path="/" exact />
          <Route component={Contact} path="/contact" exact />
          <Route component={About} path="/about" exact />
          <Route component={Profile} path="/profile" exact />
        </Switch>
      </BrowserRouter>
    </Provider>
  )
}

export default App