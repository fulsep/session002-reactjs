import React from 'react'
import {Navbar, NavbarBrand, NavItem, NavLink, Nav, NavbarToggler, Collapse, Container} from 'reactstrap'
import {Link} from 'react-router-dom'


const NavigationBar = ()=> {
  const [navbarOpen, toggleNavbar] = React.useState(false)
  return(
    <Navbar color="dark" dark expand="md">
    <Container>
      <NavbarBrand>Website</NavbarBrand>
      <NavbarToggler onClick={()=>toggleNavbar(!navbarOpen)} />
      <Collapse isOpen={navbarOpen} navbar>
        <Nav className="ml-auto" navbar>
          <NavItem>
            <Link className="nav-link" to="/">Home</Link>
          </NavItem>
          <NavItem>
            <Link className="nav-link" to="/about">About</Link>
          </NavItem>
          <NavItem>
            <Link className="nav-link" to="/contact">Contact Us</Link>
          </NavItem>
          <NavItem>
            <Link className="nav-link" to="/profile">Profile</Link>
          </NavItem>
        </Nav>
      </Collapse>
    </Container>
  </Navbar>
  )
}

export default NavigationBar